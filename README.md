## Befrost

Access odin microservices easily.

To get started run `npm install --save git@https://cabsolutely@bitbucket.org/cabsolutely/befrost.git`

You can now use Befrost:
```javascript
var Befrost = require('befrost');
```

## Example
Here is an example code for a simple service call.
```javascript
var Befrost = require('befrost');

var config = {
    serviceName: 'test-service',
    hostPortList: ['127.0.0.1:21300'],
    timeout: 1000,
    secretKey: 'secret_key'
};

var testClient = Befrost(config, onConnected);

function onConnected(err, service) {
    if (err) {
        // handle error
        return;
    }
    testHealth();
}

function testHealth() {
    testClient.testHealth(function onResult(err, res) {
        if (err) {
            // handle error
            return;
        }
        console.log(res);
    });
}
```

* serviceName: The service's name
* hostPortList: The address where the hyperbahn runs.
* timeout: A default timeout for the requests (ms)

note: You can only use the service functions after the onConnected callback has been called. You can always check the connection with the isConnected property.


## Installation

`npm install --save git@https://cabsolutely@bitbucket.org/cabsolutely/befrost.git`

## Tests

npm test

## Contributors
* Balazs Fabian