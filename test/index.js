'use strict';

var TestCluster = require('./lib/test-cluster.js');

TestCluster.test('calling health', {
	appCount: 1
}, function t(cluster, assert) {
    cluster.befrostClient.onConnected(function onConnected(error) {
        assert.ifError(error);
        cluster.befrostClient.test_health(onHealth);

        function onHealth(err, resp) {
            assert.ifError(err);

            assert.ok(resp.ok);
            assert.equal(resp.body.message, 'ok');

            assert.end();
        }
    });
});

TestCluster.test('calling post', {
	appCount: 1
}, function t(cluster, assert) {
    cluster.befrostClient.onConnected(function onConnected(error) {
        assert.ifError(error);
        cluster.befrostClient.test_post({key: 'foo', value: 'bar'}, onPost);

        function onPost(err, resp) {
            assert.ifError(err);

            assert.ok(resp.ok);

            assert.end();
        }
    });
});

TestCluster.test('calling put', {
	appCount: 1
}, function t(cluster, assert) {
    cluster.befrostClient.onConnected(function onConnected(error) {
        assert.ifError(error);
        cluster.befrostClient.test_put({key: 'foo', value: 'bar'}, onPut);

        function onPut(err, resp) {
            assert.ifError(err);

            assert.ok(resp.ok);

            assert.end();
        }
    });
});

TestCluster.test('calling delete', {
	appCount: 1
}, function t(cluster, assert) {
    cluster.befrostClient.onConnected(function onConnected(error) {
        assert.ifError(error);
        cluster.befrostClient.test_delete({key: 'foo'}, onDelete);

        function onDelete(err, resp) {
            assert.ifError(err);

            assert.ok(!resp.ok);
            assert.equal(resp.body.message, 'no such key foo');

            assert.end();
        }
    });
});

TestCluster.test('create befrost client without onConnected callback', {
	appCount: 1
}, function t(cluster, assert) {

	/* eslint-disable */
    setTimeout(callHealth, 2000);
    /* eslint-enable */

    function callHealth() {
        cluster.befrostClient.test_health(onHealth);
    }

    function onHealth(err, resp) {
            assert.ifError(err);

            assert.ok(resp.ok);
            assert.equal(resp.body.message, 'ok');

            assert.end();
        }
});

TestCluster.test('calling onConnected callback after 2 seconds wait', {
    appCount: 1
}, function t(cluster, assert) {

    /* eslint-disable */
    setTimeout(callOnConnected, 2000);
    /* eslint-enable */

    function callOnConnected() {
        cluster.befrostClient.onConnected(function onConnected(error) {
            assert.ifError(error);
            cluster.befrostClient.test_health(onHealth);

            function onHealth(err, resp) {
                assert.ifError(err);

                assert.ok(resp.ok);
                assert.equal(resp.body.message, 'ok');

                assert.end();
            }
        });
    }
});

TestCluster.test('calling destroy', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.befrostClient.onConnected(function onConnected(error) {
        assert.ifError(error);
        cluster.befrostClient.destroy();
        assert.end();

        /* eslint-disable */
        process.exit();
        /* eslint-enable */
    });
});

