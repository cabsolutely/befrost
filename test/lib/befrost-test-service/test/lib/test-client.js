'use strict';

var TChannel = require('tchannel');
var HyperbahnClient = require('tchannel/hyperbahn');
var fs = require('fs');
var path = require('path');

var thriftFile = fs.readFileSync(path.join(
    __dirname, '..', '..', 'thrift', 'service.thrift'
), 'utf8');

module.exports = TestClient;

function TestClient(options) {
    if (!(this instanceof TestClient)) {
        return new TestClient(options);
    }

    var self = this;

    self.serviceName = options.serviceName;
    self.tchannel = TChannel({
        logger: options.logger
    });
    self.hyperbahnClient = HyperbahnClient({
        tchannel: self.tchannel,
        serviceName: self.serviceName + '-test',
        hostPortList: options.peers,
        logger: options.logger,
        reportTracing: false
    });

    self.tchannelThrift = self.tchannel.TChannelAsThrift({
        source: thriftFile,
        channel: self.hyperbahnClient.getClientChannel({
            serviceName: self.serviceName
        })
    });
}

TestClient.prototype.destroy = function destroy() {
    var self = this;

    self.hyperbahnClient.destroy();
    self.tchannel.close();
};

TestClient.prototype.health = function health(cb) {
    var self = this;

    self.tchannelThrift.request({
        serviceName: self.serviceName,
        hasNoParent: true
    }).send('BefrostTestService::test_health', null, null, cb);
};

TestClient.prototype.get = function get(key, cb) {
    var self = this;

    self.tchannelThrift.request({
        serviceName: self.serviceName,
        hasNoParent: true
    }).send('BefrostTestService::test_get', null, {
        key: key
    }, cb);
};

TestClient.prototype.getAll = function getAll(skip, limit, cb) {
    var self = this;

    self.tchannelThrift.request({
        serviceName: self.serviceName,
        hasNoParent: true
    }).send('BefrostTestService::test_getAll', null, {
        skip: skip,
        limit: limit
    }, cb);
};

TestClient.prototype.put = function put(key, value, cb) {
    var self = this;

    self.tchannelThrift.request({
        serviceName: self.serviceName,
        hasNoParent: true
    }).send('BefrostTestService::test_put', null, {
        key: key,
        value: value
    }, cb);
};

TestClient.prototype.post = function post(key, value, cb) {
    var self = this;

    self.tchannelThrift.request({
        serviceName: self.serviceName,
        hasNoParent: true
    }).send('BefrostTestService::test_post', null, {
        key: key,
        value: value
    }, cb);
};

TestClient.prototype.delete = function del(key, cb) {
    var self = this;

    self.tchannelThrift.request({
        serviceName: self.serviceName,
        hasNoParent: true
    }).send('BefrostTestService::test_delete', null, {
        key: key
    }, cb);
};

TestClient.prototype.getInterface = function getInterface(cb) {
    var self = this;

    self.tchannelThrift.request({
        serviceName: self.serviceName,
        hasNoParent: true
    }).send('BefrostTestService::getInterface', null, null, cb);
};

