'use strict';

/** TODO DELETE ME

This is an example test to demonstrate how to use the
test-client.js to write integration tests

*/

var TestCluster = require('./lib/test-cluster.js');

var fs = require('fs');
var path = require('path');

TestCluster.test('calling post -> get', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.client.post('foo', 'bar', onPost);

    function onPost(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.get('foo', onGet);
    }

    function onGet(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.equal(resp.body.result, 'bar');

        assert.end();
    }
});

TestCluster.test('calling get for non-existant key', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.client.get('foo', onGet);

    function onGet(err, resp) {
        assert.ifError(err);

        assert.ok(!resp.ok);
        assert.equal(resp.body.message, 'no such key foo');

        assert.end();
    }
});

TestCluster.test('calling post -> get -> put -> get', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.client.post('foo', 'bar', onPost);

    function onPost(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.get('foo', onGet1);
    }

    function onGet1(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.equal(resp.body.result, 'bar');

        cluster.client.put('foo', 'bar2', onPut);
    }

    function onPut(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.get('foo', onGet2);
    }

    function onGet2(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.equal(resp.body.result, 'bar2');

        assert.end();
    }
});

TestCluster.test('calling post -> get -> delete -> get', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.client.post('foo', 'bar', onPost);

    function onPost(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.get('foo', onGet1);
    }

    function onGet1(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.equal(resp.body.result, 'bar');

        cluster.client.delete('foo', onDelete);
    }

    function onDelete(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.get('foo', onGet2);
    }

    function onGet2(err, resp) {
        assert.ifError(err);

        assert.ok(!resp.ok);
        assert.equal(resp.body.message, 'no such key foo');

        assert.end();
    }
});

TestCluster.test('calling delete for non-existant key', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.client.delete('foo', onDelete);

    function onDelete(err, resp) {
        assert.ifError(err);

        assert.ok(!resp.ok);
        assert.equal(resp.body.message, 'no such key foo');

        assert.end();
    }
});

TestCluster.test('calling post -> post -> post -> getAll -> getAll', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.client.post('foo', 'bar', onPost1);

    function onPost1(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.post('foo2', 'bar2', onPost2);
    }

    function onPost2(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.post('foo3', 'bar3', onPost3);
    }

    function onPost3(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.getAll(0, 20, onGetAll1);
    }

    function onGetAll1(err, resp) {
        assert.ifError(err);

        assert.equal(resp.body.result.length, 3);

        assert.ok(resp.ok);
        assert.equal(resp.body.result[0], 'bar');
        assert.equal(resp.body.result[1], 'bar2');
        assert.equal(resp.body.result[2], 'bar3');

        cluster.client.getAll(1, 1, onGetAll2);
    }

    function onGetAll2(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.equal(resp.body.result.length, 1);
        assert.equal(resp.body.result[0], 'bar2');

        assert.end();
    }
});

TestCluster.test(
    'testing cache, calling post -> get -> ' +
    'get (after 4 seconds) -> get (after 8 seconds)', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.client.post('foo', 'bar', onPost);

    function onPost(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.get('foo', onGet1);
    }

    function onGet1(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.ok(resp.body.cache);

        assert.equal(resp.body.result, 'bar');

        /* eslint-disable */
        setTimeout(callGet, 4000);
        /* eslint-enable */

        function callGet() {
            cluster.client.get('foo', onGet2);
        }
    }

    function onGet2(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.ok(resp.body.cache);

        assert.equal(resp.body.result, 'bar');

        /* eslint-disable */
        setTimeout(callGet, 8000);
        /* eslint-enable */

        function callGet() {
            cluster.client.get('foo', onGet3);
        }
    }

    function onGet3(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.ok(!resp.body.cache);

        assert.equal(resp.body.result, 'bar');

        assert.end();
    }

});

TestCluster.test(
    'testing cache, calling post -> post -> getAll -> ' +
    'getAll (after 4 seconds) -> getAll (after 8 seconds)', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.client.post('foo', 'bar', onPost1);

    function onPost1(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.post('foo2', 'bar2', onPost2);
    }

    function onPost2(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);

        cluster.client.getAll(0, 20, onGetAll1);
    }

    function onGetAll1(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.ok(!resp.body.cache);

        assert.equal(resp.body.result.length, 2);
        assert.equal(resp.body.result[0], 'bar');
        assert.equal(resp.body.result[1], 'bar2');

        /* eslint-disable */
        setTimeout(callGetAll, 4000);
        /* eslint-enable */

        function callGetAll() {
            cluster.client.getAll(0, 20, onGetAll2);
        }
    }

    function onGetAll2(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.ok(resp.body.cache);

        assert.equal(resp.body.result.length, 2);
        assert.equal(resp.body.result[0], 'bar');
        assert.equal(resp.body.result[1], 'bar2');

        /* eslint-disable */
        setTimeout(callGetAll, 8000);
        /* eslint-enable */

        function callGetAll() {
            cluster.client.getAll(0, 20, onGetAll3);
        }
    }

    function onGetAll3(err, resp) {
        assert.ifError(err);

        assert.ok(resp.ok);
        assert.ok(!resp.body.cache);

        assert.equal(resp.body.result.length, 2);
        assert.equal(resp.body.result[0], 'bar');
        assert.equal(resp.body.result[1], 'bar2');

        assert.end();
    }

});

var thriftFile = fs.readFileSync(
    path.join(__dirname, '..', 'thrift', 'service.thrift'), 'utf8'
);

TestCluster.test('calling getInterface', {
    appCount: 1
}, function t(cluster, assert) {
    cluster.client.getInterface(onGetInterface);

    function onGetInterface(err, resp) {
        assert.ifError(err);
        assert.ok(resp.ok);
        assert.equal(resp.body, thriftFile);
        assert.end();

        /* eslint-disable */
        process.exit();
        /* eslint-enable */
    }
});

