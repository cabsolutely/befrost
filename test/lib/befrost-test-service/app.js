'use strict';

var fetchConfig = require('zero-config');
var ApplicationClients = require('./clients.js');

var TestStorage = require('./storage/storage-test.js');

// TODO require a storage. For example:
// var Storage = require('/storage/storage-mongo.js');

module.exports = Application;

function Application(options) {
    if (!(this instanceof Application)) {
        return new Application(options);
    }

    var self = this;
    options = options || {};

    self.config = fetchConfig(__dirname, {
        dcValue: 'todo',
        seed: options.seedConfig,
        loose: false
    });

    self.clients = ApplicationClients(self.config, {
        logger: options.logger,
        statsd: options.statsd
    });

    self.testStorage = new TestStorage(
                            5,
                            15,
                            6);

    // TODO uncomment theese
    // var defaultLimit = self.config.get('defaultLimit');
    // var maxLimit = self.config.get('maxLimit');
    // var cacheTTL = self.config.get('cacheTTL');

    // TODO set self.storage to a valid storage. For example:
    // self.storage = new Storage(
    //                     mongoUrl,
    //                     defaultLimit,
    //                     maxLimit,
    //                     cacheTTL,
    //                     options.logger);

    var channel = self.clients.appChannel;
    var thrift = self.clients.tchannelThrift;

    registerThriftFunctions(self, thrift, channel);
}

function registerThriftFunctions(app, thrift, channel) {
    thrift.register(
        channel, 'BefrostTestService::getInterface', app,
        Application.getInterface
    );

    thrift.register(
        channel, 'BefrostTestService::test_health', app,
        Application.health
    );
    thrift.register(
        channel, 'BefrostTestService::test_get', app,
        Application.get
    );
    thrift.register(
        channel, 'BefrostTestService::test_getAll', app,
        Application.getAll
    );
    thrift.register(
        channel, 'BefrostTestService::test_put', app,
        Application.put
    );
    thrift.register(
        channel, 'BefrostTestService::test_post', app,
        Application.post
    );
    thrift.register(
        channel, 'BefrostTestService::test_delete', app,
        Application.delete
    );

    // TODO register thrift functions
}

Application.prototype.bootstrap = function bootstrap(cb) {
    var self = this;

    self.clients.bootstrap(cb);
};

Application.prototype.destroy = function destroy() {
    var self = this;

    self.clients.destroy();
    self.testStorage.destroy();
    // TODO uncomment this
    // self.storage.destroy();
};

Application.getInterface = function getInterface(app, req, head, body, cb) {
    cb(null, {
        ok: true,
        body: app.clients.thriftFile
    });
};

// Test endpoints
Application.health = function health(app, req, head, body, cb) {
    cb(null, {
        ok: true,
        body: {
            message: 'ok'
        }
    });
};

Application.get = function get(app, req, head, body, cb) {
    app.testStorage.get('test', body.key, function onResult(err, res) {
        /*istanbul ignore if: database error, cannot be tested*/
        if (err) {
            cb(null, {
                ok: false,
                body: {
                    result: 'ERROR',
                    cache: false
                }
            });
        }
        if (res) {
            return cb(null, {
                ok: true,
                body: {
                    result: res.value,
                    cache: res.cache
                }
            });
        }
        cb(null, {
            ok: false,
            body: new Error('no such key ' + body.key),
            typeName: 'noKey'
        });
    });
};

Application.getAll = function getAll(app, req, head, body, cb) {
    app.testStorage.getAll(
        'test',
        {
            query: {},
            skip: body.skip,
            limit: body.limit
        },
        function onResult(err, res) {
            /*istanbul ignore if: database error, cannot be tested*/
            if (err) {
                return cb(null, {
                    ok: false,
                    body: null
                });
            }
            var resultArray = [];
            for (var i = 0; i < res.result.length; i++) {
                resultArray.push(res.result[i].value);
            }
            cb(null, {
                ok: true,
                body: {
                    result: resultArray,
                    cache: res.cache
                }
            });
        }
    );
};

Application.put = function put(app, req, head, body, cb) {
    app.testStorage.put(
        'test',
        body.key,
        {
            id: body.key,
            value: body.value
        },
        function onResult(err, res) {
            /*istanbul ignore if: database error, cannot be tested*/
            if (err) {
                return cb(null, {
                    ok: false,
                    body: null
                });
            }
            cb(null, {
                ok: true,
                body: res.id
            });
        }
    );
};

Application.post = function post(app, req, head, body, cb) {
    app.testStorage.post(
        'test',
        {
            id: body.key,
            value: body.value
        },
        function onResult(err, res) {
            /*istanbul ignore if: database error, cannot be tested*/
            if (err) {
                return cb(null, {
                    ok: false,
                    body: null
                });
            }
            cb(null, {
                ok: true,
                body: res.id
            });
        }
    );
};

Application.delete = function del(app, req, head, body, cb) {

    app.testStorage.delete(
        'test',
        body.key,
        function onResult(err, res) {
            /*istanbul ignore if: database error, cannot be tested*/
            if (err) {
                return cb(null, {
                    ok: false,
                    body: null
                });
            }
            if (res) {
                return cb(null, {
                    ok: true,
                    body: res
                });
            }
            cb(null, {
                ok: false,
                body: new Error('no such key ' + body.key),
                typeName: 'noKey'
            });
        }
    );
};
// TODO implement thrift functions

