'use strict';
var NodeCache = require('node-cache');
var hash = require('object-hash');

module.exports = Storage;

function Storage(url, defaultLimit, maxLimit, cacheTTL, logger) {
    var self = this;

    // set limit and cache values
    self.defaultLimit = defaultLimit;
    self.maxLimit = maxLimit;
    self.cacheTTL = cacheTTL;

    // initialize the cache with the given TTL
    self.cache = new NodeCache({
        stdTTL: cacheTTL
    });

    // TODO establish database connection

}

Storage.prototype.get = function get(collection, id, cb) {
    var self = this;

    // create hash key
    var key = hash({
        collection: collection,
        id: id
    });

    // check if result is already in the cache
    var result = self.cache.get(key);
    if (result) {
        // reset cached data TTL
        self.cache.ttl(key, self.cacheTTL);
        // retrive cache result
        cb(null, result);
        return;
    }

    // TODO uncomment theese
    // // create a callback for the request that includes caching
    // function callback(err, res) {
    //     if (err) {
    //         cb(err);
    //         return;
    //     }

    //     // insert get result into the cache
    //     self.cache.set(key, res);

    //     // retrive the result
    //     cb(null, res);
    // }

    // TODO retrive data from database, and call the callback above

};

Storage.prototype.getAll = function getAll(collection, filter, cb) {
    var self = this;

    // create hash key
    var key = hash({
        collection: collection,
        filter: filter
    });

    // check if result is already in the cache
    var result = self.cache.get(key);
    if (result) {
        // reset cached data ttl
        self.cache.ttl(key, self.cacheTTL);
        // retrive cache result
        cb(null, result);
        return;
    }

    // TODO uncomment theese
	// // create a callback for the request that includes caching
 //    function callback(err, res) {
 //        if (err) {
 //            cb(err);
 //            return;
 //        }

 //        // insert get result into the cache
 //        self.cache.set(key, res);

 //        // retrive the result
 //        cb(null, res);
 //    }

    // // validating the filter, fixing missing parameters
    // var query = filter.query ? filter.query : {};
    // var skip = filter.skip ? filter.skip : 0;
    // var limit = filter.limit ?
    //         filter.limit <= self.maxLimit ? filter.limit : self.maxLimit
    //         : self.defaultLimit;

    // TODO retrive data from database, and call the callback above

};

Storage.prototype.put = function put(collection, id, object, cb) {
    // TODO uncomment theese
    // var self = this;

    // // create a callback for the request that includes caching
    // function callback(err, res) {
    //     if (err) {
    //         cb(err);
    //         return;
    //     }

    //     // modify cache data if the modified object was in the cache
    //     // create hash key
    //     var key = hash({
    //         collection: collection,
    //         id: id
    //     });

    //     // insert modified result into the cache
    //     self.cache.set(key, res);

    //     // retrive the result
    //     cb(null, res);
    // }

    // TODO modify data in database, and call the callback above

};

Storage.prototype.post = function post(collection, object, cb) {
    // TODO uncomment theese
    // var self = this;

    // // create a callback for the request that includes caching
    // function callback(err, res, id) {
    //     if (err) {
    //         cb(err);
    //         return;
    //     }

    //     // create hash key
    //     var key = hash({
    //         collection: collection,
    //         id: id
    //     });

    //     // insert post result into the cache
    //     self.cache.set(key, res);

    //     // retrive the result
    //     cb(null, res);
    // }

    // TODO post object to database

};

Storage.prototype.delete = function del(collection, id, cb) {
    var self = this;

    // create hash key
    var key = hash({
        collection: collection,
        id: id
    });

    // delete object from the cache
    self.cache.del(key);

    // TODO remove object from database

};

Storage.prototype.destroy = function destroy() {
    // TODO close database connection

};
