'use strict';
var NodeCache = require('node-cache');
var hash = require('object-hash');

var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;

module.exports = Storage;

function Storage(url, defaultLimit, maxLimit, cacheTTL, logger) {
    var self = this;

    // set limit and cache values
    self.defaultLimit = defaultLimit;
    self.maxLimit = maxLimit;
    self.cacheTTL = cacheTTL;

    // initialize the cache with the given TTL
    self.cache = new NodeCache({
        stdTTL: cacheTTL
    });

    // TODO establish database connection
    MongoClient.connect(url, function onConnection(err, mongodb) {
        if (err) {
            logger.fatal('cannot establish mongodb connection', {
                err: err
            });
        }
        self.mongodb = mongodb;
    });
}

Storage.prototype.get = function get(collection, id, cb) {
    var self = this;

    // create hash key
    var key = hash({
        collection: collection,
        id: id
    });

    // check if result is already in the cache
    var result = self.cache.get(key);
    if (result) {
        // reset cached data TTL
        self.cache.ttl(key, self.cacheTTL);
        // retrive cache result
        cb(null, result);
        return;
    }

    // create a callback for the request that includes caching
    function callback(err, res) {
        if (err) {
            cb(err);
            return;
        }

        // insert get result into the cache
        self.cache.set(key, res);

        // retrive the result
        cb(null, res);
    }

    // TODO retrive data from database, and call the callback above
    var cursor = self.mongodb.collection(collection)
                        .find({
                            _id: ObjectId(id)
                        })
                        .limit(1);
    cursor.next(function onResult(err, res) {
        callback(err, res);
    });
};

Storage.prototype.getAll = function getAll(collection, filter, cb) {
    var self = this;

    // create hash key
    var key = hash({
        collection: collection,
        filter: filter
    });

    // check if result is already in the cache
    var result = self.cache.get(key);
    if (result) {
        // reset cached data ttl
        self.cache.ttl(key, self.cacheTTL);
        // retrive cache result
        cb(null, result);
        return;
    }

	// create a callback for the request that includes caching
    function callback(err, res) {
        if (err) {
            cb(err);
            return;
        }

        // insert get result into the cache
        self.cache.set(key, res);

        // retrive the result
        cb(null, res);
    }

    // validating the filter, fixing missing parameters
    var query = filter.query ? filter.query : {};
    var skip = filter.skip ? filter.skip : 0;
    var limit = filter.limit ?
                filter.limit <= self.maxLimit ? filter.limit : self.maxLimit
                : self.defaultLimit;

    // TODO retrive data from database, and call the callback above
    var cursor = self.mongodb.collection(collection)
                        .find(query)
                        .skip(skip)
                        .limit(limit);

    var results = [];

    cursor.each(function loop(err, res) {
        if (err) {
            cb(err);
            return;
        }
        if (res !== null) {
            results.push(res);
            return;
        }
        callback(null, results);
    });
};

Storage.prototype.put = function put(collection, id, object, cb) {
    var self = this;

    // create a callback for the request that includes caching
    function callback(err, res) {
        if (err) {
            cb(err);
            return;
        }

        // modify cache data if the modified object was in the cache
        // create hash key
        var key = hash({
            collection: collection,
            id: id
        });

        // insert modified result into the cache
        self.cache.set(key, res);

        // retrive the result
        cb(null, res);
    }

    // TODO modify data in database, and call the callback above
    self.mongodb.collection(collection).replaceOne({
            _id: ObjectId(id)
        },
        object,
        function onResult(err, res) {
            callback(err, res);
        }
    );

};

Storage.prototype.post = function post(collection, object, cb) {
    var self = this;

    // create a callback for the request that includes caching
    function callback(err, res, id) {
        if (err) {
            cb(err);
            return;
        }

        // create hash key
        var key = hash({
            collection: collection,
            id: id
        });

        // insert post result into the cache
        self.cache.set(key, res);

        // retrive the result
        cb(null, res);
    }

    // TODO post object to database
    self.mongodb.collection(collection).insertOne(
        object,
        function onResult(err, res) {
            callback(err, res, res.requestedId);
        }
    );
};

Storage.prototype.delete = function del(collection, id, cb) {
    var self = this;

    // create hash key
    var key = hash({
        collection: collection,
        id: id
    });

    // delete object from the cache
    self.cache.del(key);

    // TODO remove object from database
    self.mongodb.collection(collection).deleteOne({
        _id: ObjectId(id)
    },
    function callback(err, res) {
        cb(err, res);
    });
};

Storage.prototype.destroy = function destroy() {
    var self = this;
    self.mongodb.close();
};
