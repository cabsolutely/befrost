'use strict';

/*eslint-disable*/
var NodeCache = require('node-cache');
var hash = require('object-hash');

module.exports = Storage;

function Storage(defaultLimit, maxLimit, cacheTTL) {
    var self = this;

    // set limit and cache values
    self.defaultLimit = defaultLimit;
    self.maxLimit = maxLimit;
    self.cacheTTL = cacheTTL;

    // initialize the cache with the given TTL
    self.cache = new NodeCache({
        stdTTL: cacheTTL
    });

    self.exampleDb = {};
}

Storage.prototype.get = function get(collection, id, cb) {
    var self = this;

    // create hash key
    var key = hash({
        collection: collection,
        id: id
    });

    // check if result is already in the cache
    var result = self.cache.get(key);
    if (result) {
        // reset cached data TTL
        self.cache.ttl(key, self.cacheTTL);
        // retrive cache result
        result.cache = true;
        cb(null, result);
        return;
    }

    // create a callback for the request that includes caching
    function callback(err, res) {
        if (err) {
            cb(err);
            return;
        }

        // insert get result into the cache
        self.cache.set(key, res);
        // retrive the result
        var r = {
            id: res.id,
            value: res.value,
            cache: false
        };
        cb(null, r);
    }

    if (!self.exampleDb[collection]) {
        cb(null, null);
        return;
    }

    for (var i = 0; i < self.exampleDb[collection].length; i++) {
        var res = self.exampleDb[collection][i];
        if (res.id === id) {
            callback(null, res);
            return;
        }
    }
    cb(null, null);

};

Storage.prototype.getAll = function getAll(collection, filter, cb) {
    var self = this;

    // create hash key
    var key = hash({
        collection: collection,
        filter: filter
    });

    // check if result is already in the cache
    var result = self.cache.get(key);
    if (result) {
        // reset cached data ttl
        self.cache.ttl(key, self.cacheTTL);
        // retrive cache result
        var resultObject = {
            result: result,
            cache: true
        };
        cb(null, resultObject);
        return;
    }

	// create a callback for the request that includes caching
    function callback(err, res) {
        if (err) {
            cb(err);
            return;
        }

        // insert get result into the cache
        self.cache.set(key, res);

        // retrive the result
        var resObject = {
            result: res,
            cache: false
        };
        cb(null, resObject);
    }

    // validating the filter, fixing missing parameters
    var skip = filter.skip ? filter.skip : 0;
    var limit = filter.limit ?
            filter.limit <= self.maxLimit ? filter.limit : self.maxLimit
            : self.defaultLimit;

    var coll = self.exampleDb[collection].slice().splice(skip);
    var results = [];
    var n = coll.length <= limit ? coll.length : limit;
    for (var i = 0; i < n; i++) {
        var res = coll[i];
        results.push(res);
    }
    callback(null, results);

};

var currId = 0;

Storage.prototype.put = function put(collection, id, object, cb) {
    var self = this;

    // create a callback for the request that includes caching
    function callback(err, res) {
        if (err) {
            cb(err);
            return;
        }

        // modify cache data if the modified object was in the cache
        // create hash key
        var key = hash({
            collection: collection,
            id: id
        });

        // insert modified result into the cache
        self.cache.set(key, res);

        // retrive the result
        cb(null, res);
    }

    if (!self.exampleDb[collection]) {
        self.exampleDb[collection] = [];
    }

    if (id === -1) {
        id = currId++;
        object.id = id;
        self.exampleDb[collection].push(object);
        callback(null, object);
        return;
    }

    for (var i = 0; i < self.exampleDb[collection].length; i++) {
        var res = self.exampleDb[collection][i];
        if (res.id === id) {
            object.id = id;
            self.exampleDb[collection][i] = object;
            callback(null, object);
            return;
        }
    }
    object.id = id;
    self.exampleDb[collection].push(object);
    callback(null, object);
};

Storage.prototype.post = function post(collection, object, cb) {
    var self = this;

    // create a callback for the request that includes caching
    function callback(err, res) {
        if (err) {
            cb(err);
            return;
        }

        // create hash key
        var key = hash({
            collection: collection,
            id: res.id
        });

        // insert post result into the cache
        self.cache.set(key, res);

        // retrive the result
        cb(null, res);
    }

    if (!self.exampleDb[collection]) {
        self.exampleDb[collection] = [];
    }

    if (object.id) {
        self.exampleDb[collection].push(object);
        callback(null, object);
        return;
    }
    object.id = currId++;
    self.exampleDb[collection].push(object);
    callback(null, object);

};

Storage.prototype.delete = function del(collection, id, cb) {
    var self = this;

    // create hash key
    var key = hash({
        collection: collection,
        id: id
    });

    // delete object from the cache
    self.cache.del(key);

    if (!self.exampleDb[collection]) {
        cb(null, null);
        return;
    }

    for (var i = 0; i < self.exampleDb[collection].length; i++) {
        var res = self.exampleDb[collection][i];
        if (res.id === id) {
            self.exampleDb[collection].splice(i, 1);
            cb(null, 'OK');
            return;
        }
    }

    cb(null, null);

};

Storage.prototype.destroy = function destroy() {
    // TODO remove object from database

};
/*eslint-enable*/
