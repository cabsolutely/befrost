exception NoSuchKey {
    1:required string message
}

struct HealthResult {
    1:required string message
}

struct GetResult {
    1:required string result
    2:required bool cache
}

struct GetListResult {
    1:required list<string> result
    2:required bool cache
}

service BefrostTestService {
    // Required endpoint
    string getInterface()

    // Test endpoints
    HealthResult test_health()
    GetResult test_get(
        1:required string key
    ) throws (
        1:optional NoSuchKey noKey
    )
    GetListResult test_getAll(
        1:required i32 skip
        2:required i32 limit
    )
    string test_put(
        1:required string key
        2:required string value
    )
    string test_post(
        1:required string key
        2:required string value
    )
    string test_delete(
        1:required string key
    ) throws (
        1:optional NoSuchKey noKey
    )

}
