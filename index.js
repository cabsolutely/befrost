'use strict';

var DebugLogtron = require('debug-logtron');

var TChannel = require('tchannel');
var HyperbahnClient = require('tchannel/hyperbahn/');
var hash = require('object-hash');

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function Befrost(config, cb) {
    if (!(this instanceof Befrost)) {
        return new Befrost(config, cb);
    }

    var self = this;

    self.service = config.serviceName;
    self.timeout = config.timeout;
    self.isConnected = false;
    self.secretKey = config.secretKey;
    self.onConnectedCallback = cb;

    generateLogtronAndThriftNames(self);

    self.logger = DebugLogtron(self.debugLogtronName);
    self.rootChannel = TChannel({
        logger: self.logger
    });

    /*istanbul ignore next: only 1 init*/
    self.hyperbahnClient = HyperbahnClient({
        tchannel: self.rootChannel,
        serviceName: config.service + '-client',
        hostPortList: config.hostPortList,
        hardFail: config.hasOwnProperty('hardFail') ? config.hardFail : true,
        logger: self.logger
    });

    loadThriftFile(self);
}

Befrost.prototype.onConnected = function onConnected(cb) {
    var self = this;

    if (self.isConnected) {
        cb();
        return;
    }
    self.onConnectedCallback = cb;
};

Befrost.prototype.destroy = function destroy() {
    var self = this;
    self.hyperbahnClient.destroy();
    self.rootChannel.close();
    self.isConnected = false;
};

function generateLogtronAndThriftNames(client) {
    var thriftServiceName = '';
    var debugLogtronName = '';
    var serviceName = client.service;
    if (serviceName.indexOf('@') !== -1) {
        serviceName = serviceName.split('@')[1];
    }
    var serviceNameElements = serviceName.split('-');

    for (var i = 0; i < serviceNameElements.length; i++) {
        var element = serviceNameElements[i];
        thriftServiceName += capitalizeFirstLetter(element);
        debugLogtronName += element;
    }

    client.thriftServiceName = thriftServiceName;
    client.debugLogtronName = debugLogtronName;
}

function loadThriftFile(client) {
    var tempThriftFile = 'service ' + client.thriftServiceName +
                         ' {\n    string getInterface()\n}';

    var tempThrift = client.rootChannel.TChannelAsThrift({
        channel: client.hyperbahnClient.getClientChannel({
            serviceName: client.service
        }),
        source: tempThriftFile,
        logger: client.logger
    });

    var header = generateHeader(
        client.thriftServiceName,
        'getInterface',
        client.secretKey
    );

    tempThrift.request({
        serviceName: client.service,
        timeout: client.timeout,
        hasNoParent: true,
        headers: {'sk': (new Date()).getTime().toString()}
    }).send(
        client.thriftServiceName + '::getInterface',
        header,
        null,
        function onResult(err, res) {
            /*istanbul ignore if: unknown error, cannot be tested*/
            if (err || !res.ok) {
                if (!err) {
                    err = {
                        message: 'An error occured during getInterface()'
                    };
                }
                client.logger.warn(
                    'An error occured during getInterface()',
                    {
                        err: err
                    }
                );
                if (client.onConnectedCallback) {
                    client.onConnectedCallback(err);
                }
                return null;
            }
            client.thriftFile = res.body;
            client.keyThrift = client.rootChannel.TChannelAsThrift({
                channel: client.hyperbahnClient.getClientChannel({
                    serviceName: client.service
                }),
                source: client.thriftFile,
                logger: client.logger,
                allowOptionalArguments: true
            });
            generateFunctions(client.thriftFile, client);
            client.isConnected = true;
            if (client.onConnectedCallback) {
                client.onConnectedCallback(null, client);
            }
        }
    );
}

function generateFunctions(thriftFile, client) {
    var thriftFileSplitted = thriftFile.split('(');
    for (var i = 0; i < thriftFileSplitted.length - 1; i++) {
        var temp = thriftFileSplitted[i];
        var tempArray = temp.split(' ');
        temp = tempArray[tempArray.length - 1];
        if (temp !== '') {
            var thriftFuncName = client.thriftServiceName + '::' + temp;
            createFunction(client, temp, thriftFuncName);
        }
    }
}

function createFunction(client, funcName, thriftFuncName) {
    client[funcName] = function func(options, sk, cb) {
        if (typeof options === 'function') {
            cb = options;
            options = null;
        }
        if (typeof sk === 'function') {
            cb = sk;
            sk = null;
        }
        if (!options) {
            options = {};
        }
        if (!sk) {
            sk = (new Date()).getTime().toString();
        }
        var header = generateHeader(
            client.thriftServiceName,
            funcName,
            client.secretKey
        );
        client.keyThrift.request({
			serviceName: client.service,
			timeout: client.timeout,
			hasNoParent: true,
            headers: {'sk': sk}
		}).send(
            thriftFuncName,
            header,
            options,
            cb
        );
    };
}

function generateHeader(thriftServiceName, method, secretKey) {
    var timestamp = new Date().getTime().toString();
    var temp = {
        service: thriftServiceName,
        method: method,
        secretKey: secretKey,
        timestamp: timestamp
    };
    var header = {
        timestamp: timestamp,
        apiKey: hash(temp)
    };
    return header;
}

module.exports = Befrost;
